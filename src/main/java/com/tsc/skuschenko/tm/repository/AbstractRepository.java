package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.IRepository;
import com.tsc.skuschenko.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<E extends AbstractEntity>
        implements IRepository<E> {

    @NotNull
    protected List<E> entities = new ArrayList<>();

    @Override
    public void add(@NotNull final E entity) {
        entities.add(entity);
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return entities;
    }

    @Nullable
    @Override
    public E findById(@NotNull final String id) {
        return entities.stream()
                .filter(item -> id.equals(item.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void remove(@NotNull final E entity) {
        entities.remove(entity);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        final Optional<E> entity = Optional.ofNullable(findById(id));
        entity.ifPresent(entities::remove);
        return entity.orElse(null);
    }

}
