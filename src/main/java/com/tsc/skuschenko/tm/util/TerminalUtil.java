package com.tsc.skuschenko.tm.util;

import com.tsc.skuschenko.tm.exception.system.IndexIncorrectException;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @NotNull final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (@NotNull final Exception e) {
            throw new IndexIncorrectException(value);
        }
    }

}
