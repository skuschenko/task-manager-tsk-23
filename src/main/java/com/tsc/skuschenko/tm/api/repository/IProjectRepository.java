package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.model.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {

}
