package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ITaskService extends IBusinessService<Task> {

    @NotNull
    Task add(
            @NotNull String userId, @Nullable String name,
            @Nullable String description
    );

}
