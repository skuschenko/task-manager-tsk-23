package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class ProjectStartByIdCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "start project by id";

    @NotNull
    private final String NAME = "project-start-by-id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId =
                serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("id");
        @NotNull final String value = TerminalUtil.nextLine();
        @NotNull final IProjectService projectService
                = serviceLocator.getProjectService();
        @Nullable final Project project =
                projectService.startById(userId, value);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
